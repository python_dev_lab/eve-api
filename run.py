# -*- coding: utf-8 -*-
import os
from eve import Eve
from flask_bootstrap import Bootstrap
from eve_docs import eve_docs

# We want to seamlessly run our API both locally and on Heroku. If running on
# AWS lambda, sensible DB connection settings are stored in environment variables.
MONGO_HOST = os.environ.get('MONGO_HOST', 'localhost')
MONGO_PORT = os.environ.get('MONGO_PORT', 27017)
MONGO_USERNAME = os.environ.get('MONGO_USERNAME', 'user')
MONGO_PASSWORD = os.environ.get('MONGO_PASSWORD', 'pass')
MONGO_DBNAME = os.environ.get('MONGO_DBNAME', 'dbname')

api_settings = {
    'MONGO_HOST': MONGO_HOST,
    'MONGO_PORT': MONGO_PORT,
    'MONGO_USERNAME' : MONGO_USERNAME,
    'MONGO_PASSWORD' : MONGO_PASSWORD,
    'MONGO_DBNAME': MONGO_DBNAME,
    'URL_PREFIX' : 'api',
    'API_VERSION' : 'v1',
    'RESOURCE_METHODS' : ['GET', 'POST', 'DELETE'],
    'ITEM_METHODS' : ['GET', 'PATCH', 'DELETE'],
    'EXTENDED_MEDIA_INFO' : ['content_type', 'name', 'length'],
    'RETURN_MEDIA_AS_BASE64_STRING' : False,
    'RETURN_MEDIA_AS_URL': True,
    'CACHE_CONTROL' : 'max-age=20',
    'CACHE_EXPIRES' : 20,
    'DOMAIN' : {'people': {
                'item_title': 'person',
                'additional_lookup':
                    {
                        'url': 'regex("[\w]+")',
                        'field': 'firstname'
                    },
                'schema':
                    {
                        'firstname': {
                            'type': 'string',
                            'minlength': 1,
                            'maxlength': 10,
                        },
                        'lastname': {
                            'type': 'string',
                            'minlength': 1,
                            'maxlength': 15,
                            'required': True,
                            'unique': True,
                        },
                    }
                },
                'accounts' :
                    {
                    'schema':
                        {
                            'name': {'type': 'string'},
                            'pic': {'type': 'media'},
                        }
                    }
            }
}


app = Eve(settings=api_settings)

Bootstrap(app)
app.register_blueprint(eve_docs,  url_prefix='/api/v1/docs')

# app.run()   # uncommment this line for local development
