import json
import logging
import time
import uuid
from pymongo import MongoClient

client = MongoClient('mongodb://devil:devil@ds131480.mlab.com:31480/devilmongodb')
db = client.devilmongodb

def create(event, context):
    data = json.loads(event['body'])
    if 'text' not in data:
        logging.error("Validation Failed")
        raise Exception("Couldn't create the todo item.")
        return

    timestamp = int(time.time() * 1000)

    item = {
        'id': str(uuid.uuid1()),
        'text': data['text'],
        'checked': False,
        'createdAt': timestamp,
        'updatedAt': timestamp,
    }

    db.items.insert_one(item)

    # create a response
    response = {
        "statusCode": 200,
        "body": json.dumps(item)
    }

    return response
